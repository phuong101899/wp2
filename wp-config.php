<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']7YQWkQ,TaeXp9Ywdw.@1}IWj{kS%{mM=0 1zc@M1zxFcDlDGw-.bB@v/,o.e,mE');
define('SECURE_AUTH_KEY',  ' V&?|R.I& 8{?d4s2w*Twbm?%&hoti-EnoZLp3);qqctep-h-Kq#=vmh=uL%UbVA');
define('LOGGED_IN_KEY',    'VK6YZhP?4EIR$@H5z~V/j%(iU9(F~pzF>)C88D3St/SqrYlElJIdP]Jxy-l^}Xpl');
define('NONCE_KEY',        'M7,q3*EuNIq~~nc@moBRdhXiQ+sT!i1d&PRh{;c5~?/zoS!$E?J/8Z&P{Ee8)7Ve');
define('AUTH_SALT',        '|y-(j5!/fZh1zWi4|55%;2&#Fs5cgj50QU=&KS]iRno)xAW@4O(0a,)erb$YY8Q!');
define('SECURE_AUTH_SALT', '^w@(FNn1NT6Y/c%},W2O]GQh-ptT[]W/X5-r,s&rxU{8%8vdnmZ)VWh6[F]h<;^i');
define('LOGGED_IN_SALT',   '=vt8|O$#{}/4roL}H_:)lx]b#E9b`oXqd7)HC;[59<zgWpp[H=;-otM`h,ry5_$u');
define('NONCE_SALT',       'S9()SnFl,JjCHW[|1RL:sf3HHe60Ez?Bt^m1aY3:lC8.1Mh9g/h;r|I5twX)~yM%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
